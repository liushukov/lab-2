## Лабораторна робота 2

### Тема: "ДОСЛІДЖЕННЯ ДОДАТКОВОГО ФУНКЦІОНАЛУ В JAVA 8-17"

#### Виконав: Люшуков Кирило

## 1) Створити maven проекту з 5-ю модулями:
1. **jmp-bank-api**
2. **jmp-service-api**
3. **jmp-cloud-bank-impl**
4. **jmp-dto**
5. **jmp-cloud-service-impl**

![Скріншот1](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo1.1.png?ref_type=heads)
**Створив проєкт**
![Скріншот2](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo1.2.png?ref_type=heads)
![Скріншот3](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo1.3.png?ref_type=heads)
![Скріншот4](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo1.4.png?ref_type=heads)
![Скріншот5](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo1.5.png?ref_type=heads)
![Скріншот6](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo1.6.png?ref_type=heads)
**Створив модулі**
---
## 2) Створіть наступні класи в модулі jmp-dto:
1. [User]  
   String name;  
   String surmane;  
   LocalDate birthday;
2. [BankCard]  
   String number;  
   User user;
3. [Subscription]  
   String bankcard;  
   LocaleDate startDate;

![Скріншот7](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo2.1.png?ref_type=heads)
![Скріншот8](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo2.2.png?ref_type=heads)
**Створив класи в dto**
---
## 3) Подовжте клас BankCard за допомогою:  
CreditBankCard  
DebitBankCard  

![Скріншот9](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo3.png?ref_type=heads)
**Подовжив клас BankCard**
---
## 4) Створити enum :
[BankCardType]  
CREDIT  
DEBIT

![Скріншот10](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo4.png?ref_type=heads)
**Створив Enum**
---
## 5) Додайте інтерфейс Bank до jmp-bank-api за допомогою:  
BankCard createBankCard(User, BankCardType)

![Скріншот11](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo5.png?ref_type=heads)
**Створив інтерфейс в заданому модулі**
---
## 6) Додайте module-info.java за допомогою:  
requires jmp-dto  
export Bank interface

![Скріншот12](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo6.png?ref_type=heads)
**Створив module-info в модулі jmp-bank-api**
---
## 7) Впровадити Bank в jmp-cloud-bank-impl. Метод повинен створювати новий клас в залежності від типу

![Скріншот13](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo7.png?ref_type=heads)
**Створення відповідної карти за типом**
---

## 8) Додайте module-info.java який містить:  
requires transitive module with Bank interface  
requires jmp-dto  
export implementation of Bank interface

![Скріншот14](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo8.png?ref_type=heads)
**Створив module-info в модулі jmp-cloud-bank-impl**
---

## 9) Додайте інтерфейс Service до jmp-service-api за допомогою:  
void subscribe(BankCard);  
Optional<Subscription> getSubscriptionByBankCardNumber(String);  
List<User> getAllUsers();

![Скріншот15](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo9.png?ref_type=heads)
**Створив інтерфейс в відповідному модулі**
---

## 10) Додайте module-info.java за допомогою:  
requires jmp-dto  
export Services interface  

![Скріншот16](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo10.png?ref_type=heads)
**Створив module-info в модулі jmp-service-api**
---

## 11) Впровадити Service в jmp-cloud-service-impl. API потоку користувача. Ви можете використовувати Map або Mongo/БД для перегляду даних.  

![Скріншот17](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo11.1.png?ref_type=heads)
![Скріншот18](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo11.2.png?ref_type=heads)
**Впровадив Service в CloudServiceImpl**
---

## 12) Додайте module-info.java з налаштуваннями:
requires transitive module with Service interface  
requires jmp-dto  
export implementation of Service interfaces  

![Скріншот19](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo12.png?ref_type=heads)
**Створив module-info в модулі jmp-cloud-service-impl**
---

## 13) Використовуйте var для визначення локальних змінних, де б це не було застосовано.
## 14) Використовуйте лямбди та функції Java 8, де б це не було застосовано.
## 15) Створити модуль основного додатку.

![Скріншот20](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo13.1.png?ref_type=heads)
![Скріншот21](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo13.2.png?ref_type=heads)
**Використання var для локальних змінних, використання лямбда, модуль основного проєкту**
---

## 16) Додайте module-info.java з налаштуваннями:  
use interface  
requires module with Bank implementation  
requires module with Service implementation  
requires jmp-dto  

![Скріншот22](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo14.png?ref_type=heads)
**Створив module-info в головному модулі**
---

## 17) Приклад роботи в консолі

![Скріншот23](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo15.1.png?ref_type=heads)
![Скріншот23](https://gitlab.com/liushukov/lab-2/-/raw/main/images/photo15.2.png?ref_type=heads)
---