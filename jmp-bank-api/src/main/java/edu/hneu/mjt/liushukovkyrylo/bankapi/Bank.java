package edu.hneu.mjt.liushukovkyrylo.bankapi;

import edu.hneu.mjt.liushukovkyrylo.dto.BankCard;
import edu.hneu.mjt.liushukovkyrylo.dto.BankCardType;
import edu.hneu.mjt.liushukovkyrylo.dto.User;

public interface Bank {
    BankCard createBankCard(User user, BankCardType bankCardType);
}
