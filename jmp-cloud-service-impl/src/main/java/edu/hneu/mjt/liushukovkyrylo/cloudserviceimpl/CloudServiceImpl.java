package edu.hneu.mjt.liushukovkyrylo.cloudserviceimpl;

import edu.hneu.mjt.liushukovkyrylo.dto.BankCard;
import edu.hneu.mjt.liushukovkyrylo.dto.Subscription;
import edu.hneu.mjt.liushukovkyrylo.dto.User;
import edu.hneu.mjt.liushukovkyrylo.serviceapi.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class CloudServiceImpl implements Service {
    private final Map<String, Subscription> subscriptions = new HashMap<>();
    private final List<User> users = new ArrayList<>();
    private final List<BankCard> bankCards = new ArrayList<>();
    @Override
    public void subscribe(BankCard bankCard, LocalDate startDate) {
        var subscription = new Subscription();
        subscription.setBankcard(bankCard.getNumber());
        subscription.setStartDate(startDate);
        subscriptions.put(bankCard.getNumber(), subscription);
        if(!this.users.contains(bankCard.getUser())){
            this.users.add(bankCard.getUser());
            bankCards.add(bankCard);
        }
    }
    @Override
    public Optional<Subscription> getSubscriptionByBankCardNumber(String bankCardNumber) {
        return Optional.ofNullable(subscriptions.get(bankCardNumber));
    }
    @Override
    public List<User> getAllUsers() {
        return new ArrayList<>(users);
    }

    @Override
    public List<BankCard> getAllCards() {
        return new ArrayList<>(bankCards);
    }
}