package edu.hneu.mjt.liushukovkyrylo.dto;

public enum BankCardType {
    CREDIT,
    DEBIT
}
