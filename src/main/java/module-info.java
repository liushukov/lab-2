module lab {
    requires jmp.cloud.service.impl;
    requires jmp.dto;
    requires jmp.cloud.bank.impl;
    uses edu.hneu.mjt.liushukovkyrylo.serviceapi.Service;
    uses edu.hneu.mjt.liushukovkyrylo.bankapi.Bank;
}