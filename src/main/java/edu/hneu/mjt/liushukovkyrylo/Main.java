package edu.hneu.mjt.liushukovkyrylo;

import edu.hneu.mjt.liushukovkyrylo.bankapi.Bank;
import edu.hneu.mjt.liushukovkyrylo.cloudbankimpl.CloudBankImpl;
import edu.hneu.mjt.liushukovkyrylo.cloudserviceimpl.CloudServiceImpl;
import edu.hneu.mjt.liushukovkyrylo.dto.BankCardType;
import edu.hneu.mjt.liushukovkyrylo.dto.Subscription;
import edu.hneu.mjt.liushukovkyrylo.dto.User;
import edu.hneu.mjt.liushukovkyrylo.serviceapi.Service;

import java.time.LocalDate;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        // Створюємо екземпляр банку та сервісу
        Bank bank = new CloudBankImpl();
        Service service = new CloudServiceImpl();

        // Створюємо декілька користувачів
        var user1 = new User("Іван", "Петров", LocalDate.of(1990, 5, 15));
        var user2 = new User("Марія", "Іванова", LocalDate.of(1985, 9, 20));
        var user3 = new User("Костянтин", "Шевченко", LocalDate.of(2002, 8, 2));
        // Створюємо картки для користувачів
        var card1 = bank.createBankCard(user1, BankCardType.CREDIT);
        var card2 = bank.createBankCard(user2, BankCardType.DEBIT);
        var card3 = bank.createBankCard(user3, BankCardType.DEBIT);
        // Підписуємо користувачів на сервіс за допомогою їхніх карток
        service.subscribe(card1, LocalDate.now());
        service.subscribe(card2, LocalDate.now());
        service.subscribe(card3, LocalDate.now());
        var allCards = service.getAllCards();
        allCards.forEach(card -> {
            System.out.println("Прізвище: " + card.getUser().getSurname() + ",\nІм'я: " + card.getUser().getName() +
                    ",\nДата народження: " + card.getUser().getBirthday() + ",\nНомер карти: " + card.getNumber() +
                    ",\nБаланс карти: " + card.getBalance() + " ₴ ,\nКредитний ліміт: " + card.getCreditLimit() + " ₴\n");
        });
        // Отримуємо підписку за номером картки
        Optional<Subscription> subscription1 = service.getSubscriptionByBankCardNumber(card1.getNumber());
        Optional<Subscription> subscription2 = service.getSubscriptionByBankCardNumber(card2.getNumber());
        //Виводимо інформацію про підписки
        subscription1.ifPresent(sub -> System.out.println("\nПідписка 1:\nномер карти: " + sub.getBankcard() +
                ",\nчас реєстрації: " + sub.getStartDate()));
        subscription2.ifPresent(sub -> System.out.println("\nПідписка 2:\nномер карти: " + sub.getBankcard() +
                ",\nчас реєстрації: " + sub.getStartDate()));
    }
}
